using System;
using System.Globalization;

namespace PersistentStack
{
    /// <summary>
    /// Represents a node.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    public class Node<T> : IEquatable<Node<T>>, ICloneable, IFormattable
    {
        /// <summary>
        /// The value.
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// The next node reference.
        /// </summary>
        public Node<T> Next { get; internal set; }

        /// <summary>
        /// Constructs the new instance of Node with the particular value and next node reference.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="next">The next node reference.</param>
        public Node(T value, Node<T> next)
        {
            Value = value;
            Next = next;
        }

        /// <summary>
        /// Deconstructs the current instance of Node into two parameters.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="next">The next node reference.</param>
        public void Deconstruct(out T value, out Node<T> next)
        {
            value = Value;
            next = Next;
        }

        /// <summary>
        /// Clones the current instance of Node.
        /// </summary>
        /// <returns>The copy of the current instance of Node.</returns>
        public Node<T> Clone()
        {
            return new Node<T>(Value, Next);
        }

        /// <summary>
        /// Clones the current instance of Node.
        /// </summary>
        /// <returns>The copy of the current instance of Node.</returns>
        object ICloneable.Clone()
        {
            return Clone();
        }

        /// <summary>
        /// Evaluates whether the current instance of the Node has the same parameters as the other one.
        /// </summary>
        /// <param name="other">The other one instance of Node.</param>
        /// <returns>The evaluation result.</returns>
        public bool Equals(Node<T> other)
        {
            if (other is null)
                throw new ArgumentNullException(nameof(other));
            return Value.Equals(other.Value) && ReferenceEquals(Next, other.Next);
        }

        /// <summary>
        /// Evaluates whether the current instance of the Node has the same parameters as the other one.
        /// </summary>
        /// <param name="obj">The other one instance of Node.</param>
        /// <returns>The evaluation result.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Node<T> node))
                throw new ArgumentException(nameof(obj));
            return Equals(node);
        }

        /// <summary>
        /// Returns the string representation of the current instance of Node.
        /// </summary>
        /// <returns>The string representation of the current instance of Node.</returns>
        public override string ToString()
        {
            return ToString(null, null);
        }

        /// <summary>
        /// Returns the string representation of the current instance of Node.
        /// </summary>
        /// <returns>The string representation of the current instance of Node.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            format = format ?? DefaultFormatString;
            formatProvider = formatProvider ?? defaultFormatProvider;
            return string.Format(formatProvider, format, Value);
        }

        private const string DefaultFormatString = "{0}";
        private readonly IFormatProvider defaultFormatProvider = CultureInfo.CurrentCulture;
    }
}